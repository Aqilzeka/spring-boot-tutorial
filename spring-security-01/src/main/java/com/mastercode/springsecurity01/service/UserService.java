package com.mastercode.springsecurity01.service;

import com.mastercode.springsecurity01.dto.UserLoginRequestModel;
import com.mastercode.springsecurity01.dto.UserRefreshTokenRequestModel;
import com.mastercode.springsecurity01.dto.UserRefreshTokenResponseModel;
import com.mastercode.springsecurity01.dto.UserRegisterRequestModel;
import com.mastercode.springsecurity01.dto.UserTokenResponseModel;

public interface UserService {

  UserRefreshTokenResponseModel register(UserRegisterRequestModel requestModel);

  UserTokenResponseModel login(UserLoginRequestModel requestModel);

  UserRefreshTokenResponseModel refreshToken(UserRefreshTokenRequestModel requestModel);
}
