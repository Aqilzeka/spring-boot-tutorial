package com.mastercode.springsecurity01.service.impl;

import com.mastercode.springsecurity01.config.JwtTokenConfiguration;
import com.mastercode.springsecurity01.dto.UserLoginRequestModel;
import com.mastercode.springsecurity01.dto.UserRefreshTokenRequestModel;
import com.mastercode.springsecurity01.dto.UserRefreshTokenResponseModel;
import com.mastercode.springsecurity01.dto.UserRegisterRequestModel;
import com.mastercode.springsecurity01.dto.UserTokenResponseModel;
import com.mastercode.springsecurity01.entity.RefreshToken;
import com.mastercode.springsecurity01.entity.Role;
import com.mastercode.springsecurity01.entity.User;
import com.mastercode.springsecurity01.mapper.UserMapper;
import com.mastercode.springsecurity01.repository.RefreshTokenRepository;
import com.mastercode.springsecurity01.repository.UserRepository;
import com.mastercode.springsecurity01.service.UserService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserMapper userMapper = UserMapper.INSTANCE;

  private final UserRepository userRepository;
  private final RefreshTokenRepository refreshTokenRepository;

  private final PasswordEncoder passwordEncoder;
  private final JwtTokenConfiguration tokenConfiguration;
  private final AuthenticationManager authenticationManager;

  @Override
  public UserRefreshTokenResponseModel register(UserRegisterRequestModel requestModel) {

    Optional<User> user = userRepository.findByEmail(requestModel.getEmail());

    if (user.isPresent()) {
      throw new RuntimeException("User already exists.");
    }

    User mappedUser = userMapper.mapToUserEntity(requestModel);
    mappedUser.setPassword(passwordEncoder.encode(requestModel.getPassword()));
    mappedUser.setRole(Role.USER);

    userRepository.save(mappedUser);

    log.info("User saved successfully");

    String accessToken = tokenConfiguration.createToken(mappedUser.getUsername());
    String refreshToken = tokenConfiguration.refreshToken(mappedUser.getUsername());

    RefreshToken refreshedToken = RefreshToken.builder()
        .token(refreshToken)
        .user(mappedUser)
        .build();

    refreshTokenRepository.save(refreshedToken);

    return UserRefreshTokenResponseModel.create(accessToken, refreshToken);
  }

  @Override
  public UserTokenResponseModel login(UserLoginRequestModel requestModel) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            requestModel.getEmail(),
            requestModel.getPassword()
        )
    );

    log.info("User successfully authenticated");

    User user = userRepository.findByEmail(requestModel.getEmail())
        .orElseThrow(() -> new UsernameNotFoundException("User not found."));

    String token = tokenConfiguration.createToken(user.getUsername());

    log.info("User logged in successfully");
    return UserTokenResponseModel.create(token);
  }

  @Override
  public UserRefreshTokenResponseModel refreshToken(UserRefreshTokenRequestModel requestModel) {
    int saltLength = 7;
    String username = tokenConfiguration.extractUsername(requestModel.getRefreshToken())
        .substring(saltLength);

    log.info("Username: " + username);

    RefreshToken refreshToken = refreshTokenRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException("User not found."));

    String accessToken = tokenConfiguration.createToken(username);
    String refreshedToken = tokenConfiguration.refreshToken(username);

    refreshToken.setToken(refreshedToken);
    refreshTokenRepository.save(refreshToken);

    return UserRefreshTokenResponseModel.create(accessToken, refreshedToken);
  }
}
