package com.mastercode.springsecurity01.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
public class UserRefreshTokenResponseModel {

  private String token;
  private String refreshToken;
}
