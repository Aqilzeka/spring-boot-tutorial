package com.mastercode.springsecurity01.mapper;

import com.mastercode.springsecurity01.dto.UserLoginRequestModel;
import com.mastercode.springsecurity01.dto.UserRegisterRequestModel;
import com.mastercode.springsecurity01.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User mapToUserEntity(UserRegisterRequestModel requestModel);

    User mapToUserEntity(UserLoginRequestModel requestModel);
}
