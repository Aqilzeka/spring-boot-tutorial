package com.mastercode.springsecurity01.repository;

import com.mastercode.springsecurity01.entity.RefreshToken;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

  @Query("SELECT t from RefreshToken t where t.user.email = :username")
  Optional<RefreshToken> findByUsername(@Param("username") String username);
}
