package com.mastercode.springsecurity01.entity;

public enum Role {
    USER, ADMIN
}
