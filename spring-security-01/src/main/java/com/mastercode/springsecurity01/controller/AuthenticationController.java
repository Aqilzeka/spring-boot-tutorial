package com.mastercode.springsecurity01.controller;

import com.mastercode.springsecurity01.dto.UserLoginRequestModel;
import com.mastercode.springsecurity01.dto.UserRefreshTokenRequestModel;
import com.mastercode.springsecurity01.dto.UserRefreshTokenResponseModel;
import com.mastercode.springsecurity01.dto.UserRegisterRequestModel;
import com.mastercode.springsecurity01.dto.UserTokenResponseModel;
import com.mastercode.springsecurity01.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class AuthenticationController {

  private final UserService userService;

  @GetMapping("/test")
  public ResponseEntity<String> test() {
    return ResponseEntity.ok("test");
  }

  @PostMapping("/register")
  public ResponseEntity<UserRefreshTokenResponseModel> register(
      @RequestBody UserRegisterRequestModel requestModel) {
    return ResponseEntity.ok(userService.register(requestModel));
  }

  @PostMapping("/login")
  public ResponseEntity<UserTokenResponseModel> register(
      @RequestBody UserLoginRequestModel requestModel) {
    return ResponseEntity.ok(userService.login(requestModel));
  }

  @PostMapping("/refresh")
  public ResponseEntity<UserRefreshTokenResponseModel> refreshToken(
      @RequestBody UserRefreshTokenRequestModel requestModel) {
    return ResponseEntity.ok(userService.refreshToken(requestModel));
  }

}
