package com.codemaster.jpa1.entity_relationships.many_to_one;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer eid;
    private String ename;
    private Double salary;
    private String deg;
    @ManyToOne
    private Department department;
}
