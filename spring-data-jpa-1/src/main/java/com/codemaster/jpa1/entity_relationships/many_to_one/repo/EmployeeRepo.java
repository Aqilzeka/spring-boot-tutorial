package com.codemaster.jpa1.entity_relationships.many_to_one.repo;

import com.codemaster.jpa1.entity_relationships.many_to_one.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepo extends JpaRepository<Employee, Integer> {
}
