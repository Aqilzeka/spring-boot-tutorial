package com.codemaster.jpa1.entity_relationships.many_to_many;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    @ManyToMany(targetEntity = Teacher.class)
    private Set<Teacher> teachers;

    public Class(String name, Set<Teacher> teachers) {
        this.name = name;
        this.teachers = teachers;
    }

    public Class(String name) {
        this.name = name;
    }
}
