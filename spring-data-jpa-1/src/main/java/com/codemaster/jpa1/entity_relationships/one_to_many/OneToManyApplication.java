package com.codemaster.jpa1.entity_relationships.one_to_many;


import com.codemaster.jpa1.entity_relationships.one_to_many.Department;
import com.codemaster.jpa1.entity_relationships.one_to_many.Employee;
import com.codemaster.jpa1.entity_relationships.one_to_many.repo.DepartmentRepo;
import com.codemaster.jpa1.entity_relationships.one_to_many.repo.EmployeeRepo;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@SpringBootApplication
public class OneToManyApplication implements CommandLineRunner {

    private final DepartmentRepo departmentRepo;
    private final EmployeeRepo employeeRepo;

    public static void main(String[] args) {
        SpringApplication.run(OneToManyApplication.class, args);
    }

    @Override
    public void run(String... args) {

        //Create Employee1 Entity
        Employee employee1 = new Employee();
        employee1.setEname("Satish");
        employee1.setSalary(45000.0);
        employee1.setDeg("Technical Writer");

        //Create Employee2 Entity
        Employee employee2 = new Employee();
        employee2.setEname("Krishna");
        employee2.setSalary(45000.0);
        employee2.setDeg("Technical Writer");

        //Create Employee3 Entity
        Employee employee3 = new Employee();
        employee3.setEname("Masthanvali");
        employee3.setSalary(50000.0);
        employee3.setDeg("Technical Writer");

        //Store Employee
        employeeRepo.save(employee1);
        employeeRepo.save(employee2);
        employeeRepo.save(employee3);

        //Create EmployeeList
        List<Employee> employees = new ArrayList();
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);

        //Create Department Entity
        Department department = new Department();
        department.setName("Development");
        department.setEmployees(employees);

        //Store Department
        departmentRepo.save(department);
    }
}
