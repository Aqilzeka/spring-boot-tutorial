package com.codemaster.jpa1.entity_relationships.one_to_many.repo;

import com.codemaster.jpa1.entity_relationships.one_to_many.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepo extends JpaRepository<Department, Integer> {
}
