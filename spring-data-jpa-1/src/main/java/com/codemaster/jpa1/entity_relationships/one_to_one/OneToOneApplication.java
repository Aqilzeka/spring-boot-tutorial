package com.codemaster.jpa1.entity_relationships.one_to_one;


import com.codemaster.jpa1.entity_relationships.one_to_one.Department;
import com.codemaster.jpa1.entity_relationships.one_to_one.Employee;
import com.codemaster.jpa1.entity_relationships.one_to_one.repo.DepartmentRepo;
import com.codemaster.jpa1.entity_relationships.one_to_one.repo.EmployeeRepo;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@SpringBootApplication
public class OneToOneApplication implements CommandLineRunner {

    private final DepartmentRepo departmentRepo;
    private final EmployeeRepo employeeRepo;

    public static void main(String[] args) {
        SpringApplication.run(OneToOneApplication.class, args);
    }

    @Override
    public void run(String... args) {
        //Create Department Entity
        Department department = new Department();
        department.setName("Development");

        //Store Department
        departmentRepo.save(department);

        //Create Employee1 Entity
        Employee employee1 = new Employee();
        employee1.setEname("Satish");
        employee1.setSalary(45000.0);
        employee1.setDeg("Technical Writer");
        employee1.setDepartment(department);

        //Store Employee
        employeeRepo.save(employee1);
    }
}
