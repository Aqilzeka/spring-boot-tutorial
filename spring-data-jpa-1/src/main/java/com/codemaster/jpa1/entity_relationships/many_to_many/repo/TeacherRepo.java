package com.codemaster.jpa1.entity_relationships.many_to_many.repo;

import com.codemaster.jpa1.entity_relationships.many_to_many.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepo extends JpaRepository<Teacher, Integer> {
}
