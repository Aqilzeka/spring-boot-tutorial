package com.codemaster.jpa1.entity_relationships.many_to_one;


import com.codemaster.jpa1.entity_relationships.many_to_one.repo.DepartmentRepo;
import com.codemaster.jpa1.entity_relationships.many_to_one.repo.EmployeeRepo;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@AllArgsConstructor
@SpringBootApplication
public class ManyToOneApplication implements CommandLineRunner {

    private final DepartmentRepo departmentRepo;
    private final EmployeeRepo employeeRepo;

    public static void main(String[] args) {
        SpringApplication.run(ManyToOneApplication.class, args);
    }

    @Override
    public void run(String... args) {
        //Create Department Entity
        Department department = new Department();
        department.setName("Development");

        //Store Department
        departmentRepo.save(department);

        //Create Employee1 Entity
        Employee employee1 = new Employee();
        employee1.setEname("Satish");
        employee1.setSalary(45000.0);
        employee1.setDeg("Technical Writer");
        employee1.setDepartment(department);

        //Create Employee2 Entity
        Employee employee2 = new Employee();
        employee2.setEname("Krishna");
        employee2.setSalary(45000.0);
        employee2.setDeg("Technical Writer");
        employee2.setDepartment(department);

        //Create Employee3 Entity
        Employee employee3 = new Employee();
        employee3.setEname("Masthanvali");
        employee3.setSalary(50000.0);
        employee3.setDeg("Technical Writer");
        employee3.setDepartment(department);

        employeeRepo.save(employee1);
        employeeRepo.save(employee2);
        employeeRepo.save(employee3);


    }
}
