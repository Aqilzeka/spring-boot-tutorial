package com.codemaster.jpa1.entity_relationships.one_to_many.repo;

import com.codemaster.jpa1.entity_relationships.one_to_many.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepo extends JpaRepository<Employee, Integer> {
}
