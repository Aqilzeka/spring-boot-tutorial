package com.codemaster.jpa1.entity_relationships.many_to_many;

import com.codemaster.jpa1.entity_relationships.many_to_many.repo.ClassRepo;
import com.codemaster.jpa1.entity_relationships.many_to_many.repo.TeacherRepo;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@SpringBootApplication
public class ManyToManyApplication implements CommandLineRunner {

    private final TeacherRepo teacherRepo;
    private final ClassRepo classRepo;

    public static void main(String[] args) {
        SpringApplication.run(ManyToManyApplication.class, args);
    }

    @Override
    public void run(String... args) {
        //Create Clas Entity
        Class class1 = new Class("1st");
        Class class2 = new Class( "2nd");
        Class class3 = new Class( "3rd");

        //Store Clas
        classRepo.save(class1);
        classRepo.save(class2);
        classRepo.save(class3);

        //Create Clas Set1
        Set<Class> classSet1 = new HashSet();
        classSet1.add(class1);
        classSet1.add(class2);
        classSet1.add(class3);

        //Create Clas Set2
        Set<Class> classSet2 = new HashSet();
        classSet2.add(class3);
        classSet2.add(class1);
        classSet2.add(class2);

        //Create Clas Set3
        Set<Class> classSet3 = new HashSet();
        classSet3.add(class2);
        classSet3.add(class3);
        classSet3.add(class1);

        //Create Teacher Entity
        Teacher teacher1 = new Teacher("Satish", "Java", classSet1);
        Teacher teacher2 = new Teacher( "Krishna", "Adv Java", classSet2);
        Teacher teacher3 = new Teacher( "Masthanvali", "DB2", classSet3);

        //Store Teacher
        teacherRepo.save(teacher1);
        teacherRepo.save(teacher2);
        teacherRepo.save(teacher3);
    }
}
