package com.codemaster.jpa1.entity_relationships.many_to_many;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String subject;

    @ManyToMany(targetEntity = Class.class)
    private Set<Class> classes;

    public Teacher(String name, String subject, Set<Class> classes) {
        this.name = name;
        this.subject = subject;
        this.classes = classes;
    }

    public Teacher(String name, String subject) {
        this.name = name;
        this.subject = subject;
    }
}
