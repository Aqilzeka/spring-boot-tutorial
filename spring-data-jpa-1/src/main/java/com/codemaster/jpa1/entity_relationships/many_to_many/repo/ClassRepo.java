package com.codemaster.jpa1.entity_relationships.many_to_many.repo;

import com.codemaster.jpa1.entity_relationships.many_to_many.Class;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassRepo extends JpaRepository<Class, Integer> {
}
