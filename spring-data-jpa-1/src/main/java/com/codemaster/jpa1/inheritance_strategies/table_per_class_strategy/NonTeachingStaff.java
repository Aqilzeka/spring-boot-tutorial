package com.codemaster.jpa1.inheritance_strategies.table_per_class_strategy;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class NonTeachingStaff extends Staff {
    private String areaExpertise;

    public NonTeachingStaff( String name, String areaExpertise) {
        super(name);
        this.areaExpertise = areaExpertise;
    }

    public NonTeachingStaff() {
        super();
    }
}
