package com.codemaster.jpa1.inheritance_strategies.joined_table_strategy.repo;

import com.codemaster.jpa1.inheritance_strategies.joined_table_strategy.NonTeachingStaff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NonTeachingStaffRepo extends JpaRepository<NonTeachingStaff, Integer> {
}
