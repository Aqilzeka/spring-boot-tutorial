package com.codemaster.jpa1.inheritance_strategies.joined_table_strategy;

import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@PrimaryKeyJoinColumn(referencedColumnName="sid")
public class NonTeachingStaff extends Staff {
    private String areaExpertise;

    public NonTeachingStaff( String name, String areaExpertise) {
        super(name);
        this.areaExpertise = areaExpertise;
    }

    public NonTeachingStaff() {
        super();
    }
}
