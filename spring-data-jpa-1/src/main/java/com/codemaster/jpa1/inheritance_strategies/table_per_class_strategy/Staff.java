package com.codemaster.jpa1.inheritance_strategies.table_per_class_strategy;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@Table
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Staff {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sid;
    private String name;

    public Staff(String name) {
        super();
        this.name = name;
    }

    public Staff() {
        super();
    }
}
