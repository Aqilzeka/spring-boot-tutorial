package com.codemaster.jpa1.inheritance_strategies.single_table_strategy;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@DiscriminatorValue( value="TS" )
public class NonTeachingStaff extends Staff {
    private String areaExpertise;

    public NonTeachingStaff( String name, String areaExpertise) {
        super(name);
        this.areaExpertise = areaExpertise;
    }

    public NonTeachingStaff() {
        super();
    }
}
