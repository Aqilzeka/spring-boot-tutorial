package com.codemaster.jpa1.inheritance_strategies.table_per_class_strategy.repo;

import com.codemaster.jpa1.inheritance_strategies.table_per_class_strategy.NonTeachingStaff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NonTeachingStaffRepo extends JpaRepository<NonTeachingStaff, Integer> {
}