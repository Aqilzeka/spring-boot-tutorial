package com.codemaster.jpa1.inheritance_strategies.single_table_strategy;

import jakarta.persistence.*;
import lombok.*;


@Entity
@Getter
@Setter
@Table
@DiscriminatorColumn( name = "type" )
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
public class Staff {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sid;
    private String name;

    public Staff(String name) {
        super();
        this.name = name;
    }

    public Staff() {
        super();
    }
}
