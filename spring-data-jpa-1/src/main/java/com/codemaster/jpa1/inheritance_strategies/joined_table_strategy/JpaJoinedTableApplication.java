package com.codemaster.jpa1.inheritance_strategies.joined_table_strategy;


import com.codemaster.jpa1.inheritance_strategies.joined_table_strategy.repo.NonTeachingStaffRepo;
import com.codemaster.jpa1.inheritance_strategies.joined_table_strategy.repo.TeachingStaffRepo;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@AllArgsConstructor
@SpringBootApplication
public class JpaJoinedTableApplication implements CommandLineRunner {

    private final TeachingStaffRepo teachingStaffRepo;
    private final NonTeachingStaffRepo nonTeachingStaffRepo;

    public static void main(String[] args) {
        SpringApplication.run(JpaJoinedTableApplication.class, args);
    }

    @Override
    public void run(String... args) {

        //Teaching staff entity
        TeachingStaff ts1 = new TeachingStaff("Gopal", "MSc MEd", "Maths");
        TeachingStaff ts2 = new TeachingStaff("Manisha", "BSc BEd", "English");

        //Non-Teaching Staff entity
        NonTeachingStaff nts1 = new NonTeachingStaff("Satish", "Accounts");
        NonTeachingStaff nts2 = new NonTeachingStaff("Krishna", "Office Admin");

        teachingStaffRepo.save(ts1);
        teachingStaffRepo.save(ts2);

        nonTeachingStaffRepo.save(nts1);
        nonTeachingStaffRepo.save(nts2);
    }
}
