package com.codemaster.jpa1.inheritance_strategies.joined_table_strategy.repo;

import com.codemaster.jpa1.inheritance_strategies.joined_table_strategy.TeachingStaff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeachingStaffRepo extends JpaRepository<TeachingStaff, Integer> {
}
