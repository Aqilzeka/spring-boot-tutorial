package com.codemaster.jpa1.inheritance_strategies.joined_table_strategy;

import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@PrimaryKeyJoinColumn(referencedColumnName="sid")
public class TeachingStaff extends Staff {
    private String qualification;
    private String subjectExpertise;

    public TeachingStaff(String name, String qualification, String subjectExpertise) {
        super(name);
        this.qualification = qualification;
        this.subjectExpertise = subjectExpertise;
    }

    public TeachingStaff() {
        super();
    }
}
