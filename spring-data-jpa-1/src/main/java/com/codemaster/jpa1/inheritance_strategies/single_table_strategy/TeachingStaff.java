package com.codemaster.jpa1.inheritance_strategies.single_table_strategy;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@DiscriminatorValue( value="TS" )
public class TeachingStaff extends Staff {
    private String qualification;
    private String subjectExpertise;

    public TeachingStaff(String name, String qualification, String subjectExpertise) {
        super(name);
        this.qualification = qualification;
        this.subjectExpertise = subjectExpertise;
    }

    public TeachingStaff() {
        super();
    }
}
