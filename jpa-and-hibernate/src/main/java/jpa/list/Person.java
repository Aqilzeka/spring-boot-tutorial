package jpa.list;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Person {
    @Id
    private Long id;
    private String name;

    @ElementCollection(fetch = FetchType.EAGER)
    protected Set<String> nickname = new HashSet<>();


}
