package jpa.map.v3;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
@Entity
public class Employee {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Company company;

}