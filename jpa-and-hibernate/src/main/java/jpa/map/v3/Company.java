package jpa.map.v3;

import jakarta.persistence.*;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@Entity
public class Company {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToMany(mappedBy = "company")
    @MapKeyJoinColumn(name = "employee_id")
    private Map<Employee, Address> addressesByEmployee;

    // constructors, getters, setters
}