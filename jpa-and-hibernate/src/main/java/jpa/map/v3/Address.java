package jpa.map.v3;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
@Entity
public class Address {
    @Id
    private Long id;
    
    @ManyToOne
    private Employee employee;
    
}