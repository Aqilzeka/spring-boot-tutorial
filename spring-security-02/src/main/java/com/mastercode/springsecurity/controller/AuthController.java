package com.mastercode.springsecurity.controller;

import com.mastercode.springsecurity.dto.request.LoginRequest;
import com.mastercode.springsecurity.dto.request.SignupRequest;
import com.mastercode.springsecurity.dto.response.MessageResponse;
import com.mastercode.springsecurity.dto.response.UserInfoResponse;
import com.mastercode.springsecurity.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

  private final UserService userService;

  @PostMapping("/sign-in")
  public ResponseEntity<UserInfoResponse> authenticateUser(
      @Valid @RequestBody LoginRequest loginRequest) {

    UserInfoResponse userInfoResponse = userService.authenticateUser(loginRequest);

    return ResponseEntity.ok()
        .header(HttpHeaders.SET_COOKIE, userInfoResponse.getJwtCookie().toString())
        .body(userInfoResponse);
  }

  @PostMapping("/signup")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    userService.registerUser(signUpRequest);
    return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
  }

  @PostMapping("/sign-out")
  public ResponseEntity<?> logoutUser() {
    return ResponseEntity.ok()
        .header(HttpHeaders.SET_COOKIE, userService.logoutUser().toString())
        .body(new MessageResponse("You've been signed out!"));
  }
}