package com.mastercode.springsecurity.service;

import com.mastercode.springsecurity.dto.request.LoginRequest;
import com.mastercode.springsecurity.dto.request.SignupRequest;
import com.mastercode.springsecurity.dto.response.UserInfoResponse;
import com.mastercode.springsecurity.entity.ERole;
import com.mastercode.springsecurity.entity.Role;
import com.mastercode.springsecurity.entity.User;
import com.mastercode.springsecurity.repository.RoleRepository;
import com.mastercode.springsecurity.repository.UserRepository;
import com.mastercode.springsecurity.security.JwtUtils;
import com.mastercode.springsecurity.security.UserDetailsImpl;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseCookie;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserService {

  private final AuthenticationManager authenticationManager;

  private final RoleRepository roleRepository;
  private final UserRepository userRepository;

  private final PasswordEncoder encoder;

  private final JwtUtils jwtUtils;

  @Override
  public UserInfoResponse authenticateUser(LoginRequest loginRequest) {
    Authentication authentication = authenticationManager
        .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
            loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

    ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

    List<String> roles = userDetails.getAuthorities().stream()
        .map(GrantedAuthority::getAuthority)
        .toList();

    return UserInfoResponse.builder()
        .id(userDetails.getId())
        .email(userDetails.getEmail())
        .username(userDetails.getUsername())
        .roles(roles)
        .jwtCookie(jwtCookie)
        .build();
  }

  @Override
  public void registerUser(SignupRequest signUpRequest) {
    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      throw new RuntimeException("Username is already taken!");
    }

    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      throw new RuntimeException("Error: Email is already in use!");
    }

    // Create new user's account
    User user = new User(signUpRequest.getUsername(),
        signUpRequest.getEmail(),
        encoder.encode(signUpRequest.getPassword()));

    Set<String> strRoles = signUpRequest.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role userRole = roleRepository.findByName(ERole.ROLE_USER)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "admin" -> {
            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);
          }
          case "mod" -> {
            Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(modRole);
          }
          default -> {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
          }
        }
      });
    }

    user.setRoles(roles);
    userRepository.save(user);
  }

  @Override
  public ResponseCookie logoutUser() {
    return jwtUtils.getCleanJwtCookie();
  }
}
