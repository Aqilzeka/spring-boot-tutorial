package com.mastercode.springsecurity.service;

import com.mastercode.springsecurity.dto.request.LoginRequest;
import com.mastercode.springsecurity.dto.request.SignupRequest;
import com.mastercode.springsecurity.dto.response.UserInfoResponse;
import org.springframework.http.ResponseCookie;

public interface UserService {

  UserInfoResponse authenticateUser(LoginRequest loginRequest);

  void registerUser(SignupRequest signUpRequest);

  ResponseCookie logoutUser();
}
