package com.mastercode.springsecurity.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.ResponseCookie;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoResponse {

  private Long id;
  private String username;
  private String email;
  private List<String> roles;

  @JsonIgnore
  private ResponseCookie jwtCookie;
}
