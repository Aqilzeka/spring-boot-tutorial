package com.mastercode.springsecurity.repository;


import com.mastercode.springsecurity.entity.ERole;
import com.mastercode.springsecurity.entity.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

  Optional<Role> findByName(ERole name);
}