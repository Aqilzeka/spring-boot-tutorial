package com.eazybytes.eazybytes.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

//  @Bean
//  public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
//    http.authorizeHttpRequests(request -> {
//              request.requestMatchers("/accounts", "/balances", "/loans", "/cards").authenticated();
//              request.requestMatchers("/notices", "/contact").permitAll();
//            }
//        )
//        .formLogin()
//        .and().httpBasic();
//
//    return http.build();
//  }

  @Bean
  public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
    http.authorizeHttpRequests()
        .requestMatchers("/accounts", "/balances", "/loans", "/cards").authenticated()
        .requestMatchers("/notices", "/contact").permitAll()
        .and().formLogin()
        .and().httpBasic();

    return http.build();
  }
}
