package com.eazybytes.eazybytes.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EazybytesSecurity01Application {

	public static void main(String[] args) {
		SpringApplication.run(EazybytesSecurity01Application.class, args);
	}

}
