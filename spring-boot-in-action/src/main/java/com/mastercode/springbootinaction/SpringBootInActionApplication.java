package com.mastercode.springbootinaction;

import com.mastercode.springbootinaction.aop.right.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

@SpringBootApplication
public class SpringBootInActionApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SpringBootInActionApplication.class, args);

        Person person = run.getBean(Person.class);
        person.turnOn();

        XmlWebApplicationContext context = new XmlWebApplicationContext();
        context.setConfigLocation("/WEB-INF/myapp-config.xml");

    }

}
