package com.mastercode.springbootinaction.di.loose;

public class Honda implements Car{
    @Override
    public void start() {
        System.out.println("Starting...");
    }
}
