package com.mastercode.springbootinaction.di.loose;

public class Person {
    public Car car;

    public Person(Car car) {
        this.car = car;
    }

    public void turnOn() {
        car.start();
    }
}
