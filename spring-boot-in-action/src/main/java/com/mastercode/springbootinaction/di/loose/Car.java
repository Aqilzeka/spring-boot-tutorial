package com.mastercode.springbootinaction.di.loose;

public interface Car {
    void start();
}
