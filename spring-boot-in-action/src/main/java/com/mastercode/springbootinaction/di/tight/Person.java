package com.mastercode.springbootinaction.di.tight;

public class Person {
    private BMW bmw;

    public Person(BMW bmw) {
        this.bmw = new BMW();
    }

    public void turnOn() {
        bmw.start();
    }
}
