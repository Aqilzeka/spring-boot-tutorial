package com.mastercode.springbootinaction.di.loose;

public class BMW implements Car{
    @Override
    public void start() {
        System.out.println("Starting...");
    }
}
