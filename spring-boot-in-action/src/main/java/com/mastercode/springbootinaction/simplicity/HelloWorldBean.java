package com.mastercode.springbootinaction.simplicity;

public class HelloWorldBean {
    public String sayHello() {
        return "Hello World";
    }
}