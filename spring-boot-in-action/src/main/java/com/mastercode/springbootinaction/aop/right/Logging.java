package com.mastercode.springbootinaction.aop.right;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Logging {

    @Before("execution(* com.mastercode.springbootinaction.aop.right.Person.turnOn())")
    public void beforeStart() {
        System.out.println("Car starting...");
    }


    @After("execution(* com.mastercode.springbootinaction.aop.right.Person.turnOn())")
    public void afterStart() {
        System.out.println("Car started...");
    }

}
