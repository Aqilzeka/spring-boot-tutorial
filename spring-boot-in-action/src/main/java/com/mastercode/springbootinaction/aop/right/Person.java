package com.mastercode.springbootinaction.aop.right;


import org.springframework.stereotype.Component;

@Component
public class Person {


    private Car car;

    public Person(BMW bmw) {
        this.car = bmw;
    }
    public void turnOn() {
        car.start();
    }
}
