package com.mastercode.springbootinaction.aop.right;

import org.springframework.stereotype.Component;

@Component
public class BMW implements Car {
    @Override
    public void start() {
        System.out.println("Start");
    }
}
