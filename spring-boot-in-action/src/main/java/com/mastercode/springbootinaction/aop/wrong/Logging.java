package com.mastercode.springbootinaction.aop.wrong;

public class Logging {

    public void beforeStart() {
        System.out.println("Car starting...");
    }

    public void afterStart() {
        System.out.println("Car started...");
    }
}
