package com.mastercode.springbootinaction.aop.wrong;


public class Person {


    private Logging logging;
    private Car car;

    public Person(Logging logging, Car car) {
        this.logging = logging;
        this.car = car;
    }

    public void turnOn() {
        logging.beforeStart();
        car.start();
        logging.afterStart();
    }
}
