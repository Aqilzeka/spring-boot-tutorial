package com.mastercode.springbootinaction.aop.right;

import org.springframework.stereotype.Component;

public interface Car {
    void start();
}
