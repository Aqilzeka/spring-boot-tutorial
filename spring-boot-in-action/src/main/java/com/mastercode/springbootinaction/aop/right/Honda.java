package com.mastercode.springbootinaction.aop.right;

import org.springframework.stereotype.Component;

@Component

public class Honda implements Car {
    @Override
    public void start() {
        System.out.println("Starting...");
    }
}
