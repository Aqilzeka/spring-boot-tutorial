package com.mastercode.springbootinaction.di.loose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PersonTest {

    @Test
    void turnOn() {
        Car mockCar = mock(Car.class);
        Person person = new Person(mockCar);
        person.turnOn();
        verify(mockCar, times(1)).start();
    }
}