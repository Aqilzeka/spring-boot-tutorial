package com.mastercode.springbeanlifecycle;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class SpringBeanLifecycleApplication {

    public static void main(String[] args) throws IOException {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringBeanLifecycleApplication.class, args);

        applicationContext
                .getBeanFactory()
                .getBeanNamesIterator()
                .forEachRemaining(System.out::println);

        List<String> movieList = Arrays.asList("Superman", "Batman", "Spiderman");
        MovieRental movieRental = applicationContext.getBean(MovieRental.class);
        movieRental.movieCheckout(movieList);
        ((BeanDefinitionRegistry) applicationContext).removeBeanDefinition("movieRental");
    }

}
