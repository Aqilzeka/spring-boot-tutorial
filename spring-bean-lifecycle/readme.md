# Spring Bean Lifecycle

In Spring, a bean is an object that is managed by the Spring container. The Spring container creates, configures, and manages the lifecycle of the beans. The lifecycle of a Spring bean consists of several stages, which are as follows:

1. Bean Definition: In this stage, the Spring container reads the XML configuration file or Java annotations and creates a BeanDefinition object. This object contains the information about the bean, such as its class name, dependencies, and scope.
2. Bean Instantiation: In this stage, the Spring container creates an instance of the bean by calling the constructor or factory method defined in the BeanDefinition. If the bean is a singleton, the container creates only one instance and caches it for future use.
3. Dependency Injection: In this stage, the Spring container injects the dependencies into the bean. The dependencies can be either primitive types, other beans, or collections of beans. The container uses either setter injection or constructor injection to inject the dependencies.
4. Bean Post Processing: In this stage, the Spring container allows developers to modify the bean instance before it is fully initialized. This can be useful for tasks like validation or custom initialization.
5. Initialization: In this stage, the Spring container invokes any initialization methods defined in the bean, such as the init-method in XML configuration or @PostConstruct annotation in Java.
6. Using the Bean: In this stage, the bean is fully initialized and can be used by other objects in the application.
7. Destruction: In this stage, the Spring container invokes any destruction methods defined in the bean, such as the destroy-method in XML configuration or @PreDestroy annotation in Java. This allows the bean to perform any necessary cleanup tasks before it is removed from the container.

Overall, the lifecycle of a Spring bean is managed by the Spring container, and developers can customize the behavior of the container using various configuration options and annotations. Understanding the lifecycle of Spring beans is essential for writing efficient and maintainable code in Spring-based applications.

## Why Would I Need to Hook into the Bean Lifecycle?

* Assigning Default values for bean properties (ex: FILE PATH, MIN_VALUE, and MAX_VALUE) while creating bean.
* Opening and Closing the Files and Database connections as part of the bean creation and destruction.
* Loading application Metadata (ex: US state code values) information while creating a bean and clean up on bean destruction.
* Starting and Terminating a Process or Thread as part of the bean creation and destruction.
* To make sure application dependency (ex: Remote Database and External Services etc...) modules are up and running while creating bean.

## [How It Works](src/main/java/com/mastercode/springbeanlifecycle/MovieRental.java)
