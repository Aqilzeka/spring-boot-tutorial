package com.selenium.hibernate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "songs")
public class Song {

    @Id
    @Column(name = "SONG_ID")
    private Long id;
    @Column(name = "SONG_NAME")
    private String songName;
    private String artist;
}
