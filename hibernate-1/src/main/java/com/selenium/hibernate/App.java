package com.selenium.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class App {
    public static void main(String[] args) {

        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(Song.class);

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        Song song = new Song();
        song.setId(1L);
        song.setSongName("Song name 1");
        song.setArtist("Elxan");

        session.beginTransaction();
        session.save(song);
        session.getTransaction().commit();

        System.out.println("Song saved ");
    }
}
