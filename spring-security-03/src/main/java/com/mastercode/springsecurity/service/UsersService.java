package com.mastercode.springsecurity.service;

import com.mastercode.springsecurity.shared.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UsersService extends UserDetailsService {

  UserDto createUser(UserDto userDetails);

  UserDto getUserDetailsByEmail(String email);
}
