package com.mastercode.graphqlplayground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.mastercode.graphqlplayground.sec01.lec04")
public class GraphqlPlaygroundApplication {

    public static void main(String[] args) {
        SpringApplication.run(GraphqlPlaygroundApplication.class, args);
    }

}
