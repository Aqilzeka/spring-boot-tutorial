package com.mastercode.graphqlplayground.sec01.lec03;

import com.mastercode.graphqlplayground.sec01.lec03.dto.Customer;
import com.mastercode.graphqlplayground.sec01.lec03.dto.CustomerOrder;
import com.mastercode.graphqlplayground.sec01.lec03.service.CustomerService;
import com.mastercode.graphqlplayground.sec01.lec03.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;

@Log4j2
@Controller
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final OrderService orderService;

//    @QueryMapping
    @SchemaMapping(typeName = "Query")
    public Flux<Customer> customers() {
        return customerService.allCustomers();
    }


    @SchemaMapping(typeName = "Customer")
    public Flux<CustomerOrder> orders(Customer customer, @Argument Integer limit) {
        log.info("Orders method invoked for {}", customer.getName());
        return orderService.ordersByCustomerName(customer.getName())
                .take(limit);
    }


}
