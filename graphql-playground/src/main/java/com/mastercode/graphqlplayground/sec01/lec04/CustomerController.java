package com.mastercode.graphqlplayground.sec01.lec04;

import com.mastercode.graphqlplayground.sec01.lec04.dto.Customer;
import com.mastercode.graphqlplayground.sec01.lec04.dto.CustomerOrder;
import com.mastercode.graphqlplayground.sec01.lec04.service.CustomerService;
import com.mastercode.graphqlplayground.sec01.lec04.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.graphql.data.method.annotation.BatchMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

@Log4j2
@Controller
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final OrderService orderService;

    //    @QueryMapping
    @SchemaMapping(typeName = "Query")
    public Flux<Customer> customers() {
        return customerService.allCustomers();
    }


//    // N + 1
//    @BatchMapping(typeName = "Customer")
//    public Flux<List<CustomerOrder>> orders(List<Customer> list) {
//        log.info("Orders method invoked for {}", list);
//        return orderService.ordersByCustomerNames(
//                list.stream().map(Customer::getName).toList()
//        );
//    }


    @BatchMapping(typeName = "Customer")
    public Mono<Map<Customer, List<CustomerOrder>>> orders(List<Customer> list) {
        log.info("Orders method invoked for {}", list);
        return orderService.fetchOrdersAsMap(list);
    }

    @SchemaMapping(typeName = "Customer")
    public Mono<Integer> age() {
        return Mono.just(100);
    }

}
