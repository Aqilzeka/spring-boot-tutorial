package com.mastercode.graphqlplayground.sec01.lec04.service;

import com.mastercode.graphqlplayground.sec01.lec04.dto.Customer;
import com.mastercode.graphqlplayground.sec01.lec04.dto.CustomerOrder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class OrderService {

    private final Map<String, List<CustomerOrder>> map = Map.of(
            "Sam", List.of(
                    CustomerOrder.create(UUID.randomUUID(), "sam-product-1"),
                    CustomerOrder.create(UUID.randomUUID(), "sam-product-2"),
                    CustomerOrder.create(UUID.randomUUID(), "sam-product-3")
            ),
            "Ali", List.of(
                    CustomerOrder.create(UUID.randomUUID(), "ali-product-1"),
                    CustomerOrder.create(UUID.randomUUID(), "ali-product-2")
            ),
            "Aysu", List.of(
                    CustomerOrder.create(UUID.randomUUID(), "Aysu-product-1"),
                    CustomerOrder.create(UUID.randomUUID(), "Aysu-product-2"),
                    CustomerOrder.create(UUID.randomUUID(), "Aysu-product-3"),
                    CustomerOrder.create(UUID.randomUUID(), "Aysu-product-4")
            )
    );

    public Flux<CustomerOrder> ordersByCustomerName(String name) {

        return Flux.fromIterable(map.getOrDefault(name, Collections.emptyList()));
    }

    public Flux<List<CustomerOrder>> ordersByCustomerNames(List<String> names) {

        return Flux.fromIterable(names)
                .flatMapSequential(name -> fetchOrders(name).defaultIfEmpty(Collections.emptyList()));
    }


    private Mono<List<CustomerOrder>> fetchOrders(String name) {
        return Mono.justOrEmpty(map.get(name))
                .delayElement(Duration.ofMillis(ThreadLocalRandom.current().nextInt(0, 500)));
    }

    public Mono<Map<Customer,List<CustomerOrder>>> fetchOrdersAsMap(List<Customer> customers) {
        return Flux.fromIterable(customers)
                .map(customer -> Tuples.of(customer, map.getOrDefault(customer.getName(), Collections.emptyList())))
                .collectMap(
                        Tuple2::getT1,
                        Tuple2::getT2
                );
    }

}
