package com.mastercode.graphqlplayground.sec01.lec03.service;

import com.mastercode.graphqlplayground.sec01.lec03.dto.CustomerOrder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class OrderService {

    private final Map<String, List<CustomerOrder>> map = Map.of(
            "Sam", List.of(
                    CustomerOrder.create(UUID.randomUUID(), "sam-product-1"),
                    CustomerOrder.create(UUID.randomUUID(), "sam-product-2"),
                    CustomerOrder.create(UUID.randomUUID(), "sam-product-3")
            ),
            "Ali", List.of(
                    CustomerOrder.create(UUID.randomUUID(), "ali-product-1"),
                    CustomerOrder.create(UUID.randomUUID(), "ali-product-2")
            ),
            "Aysu", List.of(
                    CustomerOrder.create(UUID.randomUUID(), "Aysu-product-1"),
                    CustomerOrder.create(UUID.randomUUID(), "Aysu-product-2"),
                    CustomerOrder.create(UUID.randomUUID(), "Aysu-product-3"),
                    CustomerOrder.create(UUID.randomUUID(), "Aysu-product-4")
            )
    );

    public Flux<CustomerOrder> ordersByCustomerName(String name) {

        return Flux.fromIterable(map.getOrDefault(name, Collections.emptyList()));
    }
}
