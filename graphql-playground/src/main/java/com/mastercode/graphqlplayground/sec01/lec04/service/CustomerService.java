package com.mastercode.graphqlplayground.sec01.lec04.service;

import com.mastercode.graphqlplayground.sec01.lec04.dto.Customer;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class CustomerService {

    private final Flux<Customer> flux = Flux.just(
            Customer.create(1, "Sam", 20, "Baku"),
            Customer.create(2, "Ali", 22, "Lankaran"),
            Customer.create(3, "Vali", 32, "Naxchvan"),
            Customer.create(4, "Aysu", 23, "Lerik"),
            Customer.create(5, "Aytac", 19, "Baku")
    );

    public Flux<Customer> allCustomers() {
        return flux;
    }

}
