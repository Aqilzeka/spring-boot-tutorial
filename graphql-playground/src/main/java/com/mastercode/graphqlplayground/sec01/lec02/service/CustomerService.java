package com.mastercode.graphqlplayground.sec01.lec02.service;

import com.mastercode.graphqlplayground.sec01.lec02.dto.AgeRangeFilter;
import com.mastercode.graphqlplayground.sec01.lec02.dto.Customer;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CustomerService {

    private final Flux<Customer> flux = Flux.just(
            Customer.create(1, "Sam", 20, "Baku"),
            Customer.create(2, "Ali", 22, "Lankaran"),
            Customer.create(3, "Vali", 32, "Naxchvan"),
            Customer.create(4, "Aysu", 23, "Lerik"),
            Customer.create(5, "Aytac", 19, "Baku")
    );

    public Flux<Customer> allCustomers() {
        return flux;
    }

    public Mono<Customer> customerById(Integer id) {
        return flux.filter(customer -> customer.getId().equals(id))
                .next();
    }

    public Flux<Customer> customerNameContains(String name) {
        return flux.filter(customer -> customer.getName().contains(name));
    }

    public Flux<Customer> withAge(AgeRangeFilter filter) {
        return flux
                .filter(customer ->
                        customer.getAge() >= filter.getMinAge() &&
                                customer.getAge() <= filter.getMaxAge()
                );
    }
}
